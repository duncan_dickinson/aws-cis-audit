#!/usr/bin/env bash -x

rm -rf output
mkdir output

behave --tags ~@wip --summary --no-source --logging-level ERROR --outfile output/audit-report.txt --show-skipped --junit --junit-directory output/junit

mkdir output/html
for test_file in output/junit/*.xml;
do
    base_name=$(basename "$test_file")
    out_file="${base_name%.*}"
    junit2html $test_file output/html/${out_file}.html
done
