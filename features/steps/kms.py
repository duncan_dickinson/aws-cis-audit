import boto3
from behave import *
from hamcrest import *


@then('all KMS keys must be set to rotate annually')
def step(context):
    client = boto3.client('kms')
    response = client.list_keys()

    breaches = []
    while True:
        for key in response['Keys']:
            if not client.get_key_rotation_status(KeyId=key['KeyId'])['KeyRotationEnabled']:
                breaches.append(key['KeyArn'])

        if response['Truncated']:
            response = client.list_keys(Marker=response['Marker'])
        else:
            break

    assert_that(breaches, is_(empty()))
