import boto3
import logging
from behave import *
from hamcrest import *


@given("a list of all IAM users")
def step_impl(context):
    # This is performed in the before_feature function (environment.py)
    pass


@then("no user should have an attached policy")
def step_impl(context):
    client = boto3.client('iam')
    breach_user_list = {}
    for user in context.users:
        policies = client.list_user_policies(UserName = user.name)['PolicyNames']
        if policies:
            breach_user_list[user.name] = policies

    assert_that(breach_user_list, empty())


@given("a list of all local managed policies")
def step_impl(context):
    client = boto3.client('iam')

    response = client.list_policies(Scope='Local', OnlyAttached=True)
    context.iam_policies_local = response['Policies']

    while True:
        if not response['IsTruncated']:
            break
        else:
            response = client.list_policies(Scope='Local', Marker=response['Marker'])
            context.iam_policies_local += response['Policies']


@then("the policy document must not contain an action of \"{action_test}\"")
def step_impl(context, action_test):
    client = boto3.client('iam')

    breaches = []

    for policy in context.iam_policies_local:
        policy_document = client.get_policy_version(PolicyArn=policy['Arn'],
                                                    VersionId=policy['DefaultVersionId'])['PolicyVersion']['Document']

        for statement in policy_document['Statement']:
            for action in statement['Action']:
                if action == action_test:
                    breaches.append(policy['Arn'])

    assert_that(breaches, empty())


@then('none of the group\'s local policies contain an action of \"{action_test}\"')
def step_impl(context, action_test):

    breaches = []

    for group in context.group_list:
        for policy in group.policies.all():
            for statement in policy.policy_document['Statement']:
                logging.info(statement)
                for action in statement['Action']:
                    logging.info(action)
                    if action == action_test:
                        breaches.append(group.name)

    assert_that(breaches, empty())


@then('none of the role\'s local policies contain an action of \"{action_test}\"')
def step_impl(context, action_test):

    breaches = []

    for role in context.role_list:
        for policy in role.policies.all():
            for statement in policy.policy_document['Statement']:
                logging.info(statement)
                for action in statement['Action']:
                    logging.info(action)
                    if action == action_test:
                        breaches.append(role.name)

    assert_that(breaches, empty())
