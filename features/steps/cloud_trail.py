import json

import boto3
from behave import *
from hamcrest import *


@given('all the configured Cloud Trails for an AWS Account')
def step(context):
    client = boto3.client('cloudtrail')
    context.trails = client.describe_trails()['trailList']


@given("the audit trails being multi-region, global event tracking trails")
def step_impl(context):
    context.global_trails = []
    for trail in context.trails:
        if trail['IsMultiRegionTrail'] and trail['IncludeGlobalServiceEvents']:
            context.global_trails.append(trail)


@then('there must be at least 1 audit trail')
def step(context):
    assert_that(context.global_trails, is_not(empty()))


@then("an audit trail must have log file validation enabled")
def step_impl(context):
    breaches = []
    for trail in context.global_trails:
        if not trail['LogFileValidationEnabled']:
            breaches.append(trail['Name'])

    assert_that(breaches, is_(empty()))


@then('the log bucket associated with any trail must not be publicly accessible')
def step_impl(context):
    s3 = boto3.resource('s3')

    illegal_grantees = ['http://acs.amazonaws.com/groups/global/AllUsers',
                        'http://acs.amazonaws.com/groups/global/AuthenticatedUsers']

    breaches = []
    for trail in context.trails:
        if trail['S3BucketName']:
            bucket_acl = s3.BucketAcl(trail['S3BucketName'])
            for grant in bucket_acl.grants:
                if grant['Grantee'].get('URI', None) in illegal_grantees:
                    breaches.append('{} - {}'.format(trail['Name'], 'bad ACL grant'))
                    break
            bucket_policy = json.loads(s3.BucketPolicy(trail['S3BucketName']).policy)

            for statement in bucket_policy['Statement']:
                if statement['Effect'] == 'Allow' and statement['Principal'] == '*':
                    breaches.append('{} - {}'.format(trail['Name'], 'bad bucket policy'))
                    break

    assert_that(breaches, is_(empty()))


@then('all audit trails must be integrated with CloudWatch Logs')
def step_impl(context):
    breaches = []

    for trail in context.global_trails:
        if not trail['CloudWatchLogsLogGroupArn']:
            breaches.append(trail['Name'])

    assert_that(breaches, is_(empty()))


@then('the log bucket associated with any trail must have logging enabled')
def step_impl(context):
    client = boto3.client('s3')
    breaches = []

    for trail in context.trails:
        try:
            client.get_bucket_logging(Bucket=trail['S3BucketName'])['LoggingEnabled']
        except:
            breaches.append(trail['Name'])

    assert_that(breaches, is_(empty()))


@then('all trails must use a KMS Key')
def step(context):
    breaches = []
    for trail in context.trails:
        if not trail['KmsKeyId']:
            breaches.append(trail['TrailARN'])

    assert_that(breaches, empty())
