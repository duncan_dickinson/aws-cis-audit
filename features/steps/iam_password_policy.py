import boto3
from behave import *
from hamcrest import *

@given("a password policy for an AWS account")
def step_impl(context):
    iam = boto3.resource('iam')
    context.account_password_policy = iam.AccountPasswordPolicy()

@then('the password policy requires minimum password length of {password_length:d}')
def step(context, password_length):
    assert_that(context.account_password_policy.minimum_password_length, equal_to(password_length))


@then('the password policy requires uppercase characters')
def step(context):
    assert_that(context.account_password_policy.require_uppercase_characters, is_(True))


@then('the password policy requires lowercase characters')
def step(context):
    assert_that(context.account_password_policy.require_lowercase_characters, is_(True))


@then('the password policy requires numbers')
def step(context):
    assert_that(context.account_password_policy.require_numbers, is_(True))


@then('the password policy requires symbols')
def step(context):
    assert_that(context.account_password_policy.require_symbols, is_(True))


@then('the password policy password expiration period in days is {max_password_age:d}')
def step(context, max_password_age):
    assert_that(context.account_password_policy.max_password_age, equal_to(max_password_age))


@then('the password policy prevents password reuse for {reuse_max:d} changes')
def step(context, reuse_max):
    assert_that(context.account_password_policy.password_reuse_prevention, equal_to(reuse_max))


@then('the password policy requires administrator reset for expired passwords')
def step(context):
    assert_that(context.account_password_policy.hard_expiry, is_(True))
