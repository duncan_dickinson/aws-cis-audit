import boto3
from behave import *
from hamcrest import *


@given('a list of VPCs for an account')
def step(context):
    client = boto3.client('ec2')
    response = client.describe_vpcs()
    context.vpc_list = response['Vpcs']

@given('a list of all flow logs for an account')
def step(context):
    client = boto3.client('ec2')
    response = client.describe_flow_logs()
    context.vpc_flow_logs = response['FlowLogs']

@then('all VPCs MUST have a flow log defined')
def step(context):
    breaches = []

    for vpc in context.vpc_list:
        flag = False
        for log in context.vpc_flow_logs:
            if log['ResourceId'] == vpc['VpcId']:
                flag = True
                break
        if not flag:
            breaches.append(vpc['VpcId'])

    assert_that(breaches, is_(empty()))
