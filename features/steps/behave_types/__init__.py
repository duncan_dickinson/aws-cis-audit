from behave import register_type


def parse_number(text):
    return int(text)


register_type(Number=parse_number)
