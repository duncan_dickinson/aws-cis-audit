from datetime import datetime
from behave import *
from hamcrest import *
from hamcrest.library.collection.is_empty import empty


@then("an MFA device must be enabled")
def step_impl(context):
    fails = []
    for account in context.profile_users:
        if not account.mfa_devices.all():
            fails.append(account.name)

    assert_that(fails, empty())


@then("all active access keys are less than {days} days old")
def step_impl(context, days):
    present = datetime.now().replace(tzinfo=None)
    fails = []
    for user in context.users:
        for key in user.access_keys.all():
            if key.status == 'Active':
                delta = present - key.create_date.replace(tzinfo=None)
                if delta.days > int(days):
                    fails.append({
                        'access_key_id': key.access_key_id,
                        'user': user.name,
                        'age': delta.days})

    assert_that(fails, empty())


@when("the user has access keys")
def step_impl(context):
    pass


@when("the user has a login profile")
def step_impl(context):
    pass


@then("the key creation time must not be the same as the user creation time")
def step_impl(context):
    fails = []
    for user in context.users:
        for key in user.access_keys.all():
            if (key.create_date - user.create_date).seconds < 360:
                fails.append(user.name)

    assert_that(fails, empty())
