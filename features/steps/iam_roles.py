import boto3
from behave import *

@given("a list of all roles")
def step_impl(context):
    client = boto3.client('iam')
    iam = boto3.resource('iam')

    context.role_list = []

    response = client.list_roles()
    while True:
        for item in response['Roles']:
            context.role_list.append(iam.Role(item['RoleName']))

        if not response['IsTruncated']:
            break
        else:
            response = client.list_roles(Marker=response['Marker'])


@then("the {role_name} role must exist")
def step_impl(context, role_name):
    client = boto3.client('iam')
    client.get_role(RoleName=role_name)


@then("the {role_name} role requires MFA")
def step_impl(context, role_name):
    # Not implemented
    assert False


@step("the {policy_name} managed policy is attached to the {role_name} role")
def step_impl(context, policy_name, role_name):
    # Not implemented
    assert False
