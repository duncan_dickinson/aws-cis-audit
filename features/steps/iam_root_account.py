import csv
from datetime import datetime
from time import sleep
from io import StringIO

import boto3
import pytz

from behave import *
from dateutil.parser import parse
from hamcrest import *


def generate_credential_report():
    client = boto3.client('iam')

    while client.generate_credential_report()['State'] != 'COMPLETE':
        sleep(10)

    report = client.get_credential_report()['Content']
    return csv.DictReader(StringIO(report.decode("utf-8")))


@given("the root user details")
def step_impl(context):
    if 'root_user' in context:
        return

    data = generate_credential_report()
    for record in data:
        if record['user'] == '<root_account>':
            context.root_user = record
            break

    assert 'root_user' in context


@then('the root account should not have been accessed in the last {days:d} days')
def step(context, days):
    now = datetime.now(pytz.utc)
    record_date = parse(context.root_user['password_last_used'])
    delta = now - record_date
    assert_that(delta.days, greater_than(days))


@then("the root account has no access keys")
def step_impl(context):
    assert_that(context.root_user['access_key_1_active'], is_('false'))
    assert_that(context.root_user['access_key_2_active'], is_('false'))


@then("the root account has MFA enabled")
def step_impl(context):
    assert_that(context.root_user['mfa_active'], is_('true'))


@given("a list of all Virtual MFA devices")
def step_impl(context):
    client = boto3.client('iam')
    response = client.list_virtual_mfa_devices()

    context.mfa_device_serial_numbers = []

    while True:
        for mfa in response['VirtualMFADevices']:
            context.mfa_device_serial_numbers.append(mfa['SerialNumber'])

        if not response['IsTruncated']:
            break
        else:
            response = client.list_virtual_mfa_devices(Marker=response['Marker'])


@then("the root account is not listed against a virtual MFA")
def step_impl(context):
    # The Serial Number for a root account's virtual MFA appears as:
    arn = 'arn:aws:iam::{}:mfa/root-account-mfa-device'.format(context.aws_account)
    assert_that(context.mfa_device_serial_numbers, not contains(arn))
