import boto3
from behave import given


@given("a list of all groups")
def step_impl(context):
    client = boto3.client('iam')
    iam = boto3.resource('iam')

    context.group_list = []

    response = client.list_groups()
    while True:
        for grp in response['Groups']:
            context.group_list.append(iam.Group(grp['GroupName']))

        if not response['IsTruncated']:
            break
        else:
            response = client.list_groups(Marker=response['Marker'])
