import boto3
from behave import *
from hamcrest import assert_that, empty, is_


@then('no security groups allow ingress from {cidr} to port {port}')
def step(context, cidr, port):
    client = boto3.client('ec2')
    result = client.describe_security_groups(Filters=[
        {
            'Name': 'ip-permission.to-port',
            'Values': [port]
        },
        {
            'Name': 'ip-permission.cidr',
            'Values': [cidr]
        }
    ])
    assert_that(result.get('SecurityGroups', []), is_(empty()))


@then("the {group_name} security group should have no inbound or outbound rules")
def step_impl(context, group_name):
    client = boto3.client('ec2')
    result = client.describe_security_groups(Filters=[
        {
            'Name': 'group-name',
            'Values': [group_name]
        }
    ])
    breaches = []

    for sg in result.get('SecurityGroups', []):
        if sg.get('IpPermissions', None) or sg.get('IpPermissionsEgress', None):
            breaches.append('{} - {}'.format(sg['GroupName'], sg['GroupId']))

    assert_that(breaches, is_(empty()))
