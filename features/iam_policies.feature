@iam
Feature: Application of IAM Policies
  # Enter feature description here

  @cis @level_1 @scored
  Scenario: CIS 1.16 Ensure IAM policies are attached only to groups or roles
    Given a list of all IAM users
    Then no user should have an attached policy

  @cis @level_1 @scored @review_needed
  Scenario: CIS 1.24 Ensure IAM policies that allow full "*:*" administrative privileges are not created

    Given a list of all local managed policies
    Then the policy document must not contain an action of "*:*"
    And the policy document must not contain an action of "*"

  @cis @level_1 @scored @review_needed
  Scenario: CIS 1.24 Ensure IAM policies that allow full "*:*" administrative privileges are not created - Groups
    Given a list of all groups
    Then none of the group's local policies contain an action of "*:*"
    And none of the group's local policies contain an action of "*"

  @cis @level_1 @scored @review_needed
  Scenario: CIS 1.24 Ensure IAM policies that allow full "*:*" administrative privileges are not created - Roles
    Given a list of all roles
    Then none of the role's local policies contain an action of "*:*"
    And none of the role's local policies contain an action of "*"
