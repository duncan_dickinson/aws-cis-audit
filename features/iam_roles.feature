@iam
Feature: Use of IAM roles

  @cis @level_1 @scored @review_needed
  Scenario: CIS 1.18 Ensure IAM Master roles is active

    Note that this check DOES NOT validate the permissions for the roles

    Then the IAM_Master role must exist
      And the IAM_Master role requires MFA

  @cis @level_1 @scored @review_needed
  Scenario: CIS 1.18 Ensure IAM Manager roles is active

  Note that this check DOES NOT validate the permissions for the roles

    Then the IAM_Manager role must exist
      And the IAM_Manager role requires MFA

  @cis @level_1 @scored @review_needed
  Scenario: CIS 1.22 Ensure a support role has been created to manage incidents with AWS Support
    Does not currently check the actual role policy

    Then the AWS_Support role must exist
      And the AWSSupportAccess managed policy is attached to the AWS_Support role
