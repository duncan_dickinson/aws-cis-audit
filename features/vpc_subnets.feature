@subnets @wip
Feature: VPC Subnets

  Provides a baseline set of policies for VPC Subnets

  Scenario: Subnets have a defined naming strategy
    Given a list of subnets in the vpc_name
    Then the names MUST match ...

  Scenario: Private and RDS subnets MUST NOT be attached to a route table containing a route to the internet gateway
    Given a list of subnets in the vpc_name
    When a subnet's name matches .....
    Then its associated route table MUST NOT route to an IGW

