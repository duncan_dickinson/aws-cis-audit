import boto3


def before_all(context):
    # -- SET LOG LEVEL: behave --logging-level=ERROR ...
    # on behave command-line or in "behave.ini".
    context.config.setup_logging()
    context.aws_identity = boto3.client('sts').get_caller_identity()
    context.aws_account = context.aws_identity['Account']


def get_iam_users():
    iam = boto3.client('iam')
    users = []
    user_list = iam.list_users()
    while True:
        for user in user_list['Users']:
            users.append(boto3.resource('iam').User(user['UserName']))
        if not user_list['IsTruncated']:
            break
        user_list = iam.list_users(user_list['Marker'])

    return users


def get_users_with_login_profile(context):
    profile_users = []
    for user in context.users:
        profile = user.LoginProfile()
        try:
            profile.load()
            profile_users.append(user)
        except:
            # A failed profile.load indicates the user doesn't have a login profile
            pass

    return profile_users


def before_feature(context, feature):
    if feature.name in ['IAM User configuration', 'Application of IAM Policies']:
        context.users = get_iam_users()
        context.profile_users = get_users_with_login_profile(context)
