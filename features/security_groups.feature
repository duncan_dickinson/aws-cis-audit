@security_groups
Feature: Security Group configuration

  @cis @level_1
  Scenario: CIS 4.1 Ensure no security groups allow ingress from 0.0.0.0/0 to port 22
    Then no security groups allow ingress from 0.0.0.0/0 to port 22

  @cis @level_1
  Scenario: CIS 4.2 Ensure no security groups allow ingress from 0.0.0.0/0 to port 3389
    Then no security groups allow ingress from 0.0.0.0/0 to port 3389

  @cis @level_2
  Scenario: CIS 4.4 Ensure the default security group of every VPC restricts all traffic
    Remember that security groups deny by default

    Then the default security group should have no inbound or outbound rules
