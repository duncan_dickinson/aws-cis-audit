@cloudtrail
Feature: Cloud Trail configuration

  Background:
    Given all the configured Cloud Trails for an AWS Account
    And the audit trails being multi-region, global event tracking trails

  @cis @level_1
  Scenario: CIS 2.1 Ensure CloudTrail is enabled in all regions
    Then there must be at least 1 audit trail

  @cis @level_2
  Scenario: CIS 2.2 Ensure CloudTrail log file validation is enabled
    Then an audit trail must have log file validation enabled

  @cis @level_1
  Scenario: CIS 2.3 Ensure the S3 bucket CloudTrail logs to is not publicly accessible
    Then the log bucket associated with any trail must not be publicly accessible

  @cis @level_1
  Scenario: CIS 2.4 Ensure CloudTrail trails are integrated with CloudWatch Logs
    Then all audit trails must be integrated with CloudWatch Logs

  @cis @level_1
  Scenario: 2.6 Ensure S3 bucket access logging is enabled on the CloudTrail S3 bucket
    Then the log bucket associated with any trail must have logging enabled

  @cis @level_2 @review_needed
  Scenario: CIS 2.7 Ensure CloudTrail logs are encrypted at rest using KMS CMKs
    Then all trails must use a KMS Key
