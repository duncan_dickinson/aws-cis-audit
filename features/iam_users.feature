@iam @users
Feature: IAM User configuration

  IAM Users are generally people or services.
  People usually have a login profile that allows them to access the console

  @cis @level_1 @scored
  Scenario: CIS 1.2 - Ensure multi-factor authentication (MFA) is enabled for all IAM users that have a console password
    When the user has a login profile
    Then an MFA device must be enabled

  @cis @level_1 @scored
  Scenario: CIS 1.4 - Ensure access keys are rotated every 90 days or less
    When the user has access keys
    Then all active access keys are less than 90 days old

  @cis @level_1 @scored @review_needed
  Scenario: CIS 1.23 Do not setup access keys during initial user setup for all IAM users that have a console password
    An assumption is made that the user will create a key an hour or more after the account is created

    When the user has access keys
    Then the key creation time must not be the same as the user creation time
