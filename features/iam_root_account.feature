@root_account
Feature: Root account configuration

  The Root user for an AWS account is handled differently to an IAM user.
  The AWS API doesn't provide direct access to the root user details so we need to
  load them from a credential report.

  Background:
    Given the root user details

  @cis @level_1 @scored
  Scenario: CIS 1.1 Avoid the use of the "root" account
    Then the root account should not have been accessed in the last 7 days

  @cis @level_1 @scored
  Scenario: CIS 1.12 Ensure no root account access key exists
    Then the root account has no access keys

  @cis @level_1 @scored
  Scenario: CIS 1.13 Ensure MFA is enabled for the "root" account
    Then the root account has MFA enabled

  @cis @level_2 @scored
  Scenario: CIS 1.14 Ensure hardware MFA is enabled for the "root" account
    Given a list of all Virtual MFA devices
    Then the root account has MFA enabled
    And the root account is not listed against a virtual MFA

