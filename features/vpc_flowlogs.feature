@vpc
Feature: VPC flow log configuration

  @cis @level_2
  Scenario: CIS 4.3 Ensure VPC flow logging is enabled in all VPCs
    Given a list of VPCs for an account
    And a list of all flow logs for an account
    Then all VPCs MUST have a flow log defined
