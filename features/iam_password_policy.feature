@iam
Feature: AWS Password Policy

  Background:
    Given a password policy for an AWS account

  @cis @level_1 @scored
  Scenario: CIS 1.5 - Ensure IAM password policy requires at least one uppercase letter
    Then the password policy requires uppercase characters

  @cis @level_1 @scored
  Scenario: CIS 1.6 - Ensure IAM password policy require at least one lowercase letter
    Then the password policy requires lowercase characters

  @cis @level_1 @scored
  Scenario: CIS 1.7 - Ensure IAM password policy require at least one symbol
    Then the password policy requires symbols

  @cis @level_1 @scored
  Scenario: CIS 1.8 - Ensure IAM password policy require at least one number
    Then the password policy requires numbers

  @cis @level_1 @scored
  Scenario: CIS 1.9 - Ensure IAM password policy requires minimum length of 14 or greater
    Then the password policy requires minimum password length of 14

  @cis @level_1 @scored
  Scenario: CIS 1.10 - Ensure IAM password policy prevents password reuse
    Then the password policy prevents password reuse for 24 changes

  @cis @level_1 @scored
  Scenario: CIS 1.11 - Ensure IAM password policy expires passwords within 90 days or less
    Then the password policy password expiration period in days is 90

  Scenario: GU - The password expiration requires administrator reset
    Then the password policy requires administrator reset for expired passwords
