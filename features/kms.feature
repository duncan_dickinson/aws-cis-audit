@kms
Feature: AWS Key Management Services

  @cis @level_2
  Scenario: CIS 2.8 Ensure rotation for customer created CMKs is enabled
    Then all KMS keys must be set to rotate annually
