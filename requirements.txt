behave>=1.2
boto3>=1.4
junit2html==5
PyHamcrest>=1.9
pytz==2016.10
